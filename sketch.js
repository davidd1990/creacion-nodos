// Based on
// Grokking Algorithms
// http://amzn.to/2n7KF4h
let cantidad;
let ingresar;
var graph;
let value = 0;
function setup() {
  graph = new Graph();
  var you = graph.addNode('N.O','red');
  cantidad = select('#cantidad')
  ingresar = select('#ingresar')
  let canvas =createCanvas(800, 800);
  cantidad.mousePressed(ingresarNodos)
  createGraph()
  canvas.parent('#canvas');

  ingresar.mousePressed(createGraph)
  vm.nodos.push(you)
  vm.nodosgraph.push(you)
  graph.setStart(you);
  graph.setEnd(vm.hijosnodos[vm.hijosnodos.length - 1]);
  bfs();
}
function ingresarNodos () {
  let item = graph.addNode(vm.nodo.nombre,'blue')
  vm.nodosgraph.push(item)
  vm.nodos.push(item)
}
 function createGraph () {
    for (let y = 0; y <= vm.hijosnodos.length -1 ; y++) {
      vm.hijosnodos[y].connect(...vm.hijosnodos[y].hijo)
    }

}
function draw() {
  background(51);
  graph.simulate();
  graph.show();
}


function bfs() {
  var queue = [];
  var path = [];
  queue.push(graph.start);

  while (queue.length > 0) {
    var person = queue.shift();
    if (!person.searched) {
      if (person == graph.end) {
        path.push(person);
        var next = person.parent;
        while (next) {
          path.push(next);
          next = next.parent;
        }
        console.log(path);
        break;
      } else {
        var next = person.edges;
        for (var i = 0; i < next.length; i++) {
          var neighbor = next[i];
          queue.push(neighbor);
          neighbor.parent = person;
        }
        person.searched = true;
      }
    }
  }

  for (var i = 0; i < path.length; i++) {
    path[i].highlight();
  }


}
let vm = new Vue(
    {
      el: '#app',
      data: () => ({
        nodo: {
          nombre: ''
        },
        headers: [
          { text: 'Nombre', value: 'nombre' },
          { text: 'Acciones', value: 'acciones' }
        ],
        headershijos: [
          { text: 'Nodo Origen', value: 'nombrepadre' },
          { text: 'Nodos Destinos', value: 'nombrehijos' },
          { text: 'Acciones', value: 'acciones' }
        ],
        nodosgraph: [],
        selectnodo: '',
        hijosnodos: [],
        nodos: []
      }),
      created () {
        this.traerdatos()
      },
      methods: {
        traerdatos () {

        },
        ingrearnodo () {
          let item = JSON.parse(JSON.stringify(this.nodo))
          this.nodos.push(item)
        },
        borraritem (item) {
          let index = this.nodos.indexOf(item)
          this.nodos.splice(index, 1)
        },
        borraritemhijos (item)  {
          let index = this.hijosnodos.indexOf(item)
          this.hijosnodos.splice(index, 1)
        },
        ingresarhijo() {

          this.hijosnodos.push(this.selectnodo)
        }
      }
    }
)